import React from "react";
import {Link} from "react-router-dom";

export function Navigation(){
    return(
        <nav className="h-[50px] flex justify-between px-5 bg-blue-500 text-white">
            <span className="font-bold mt-3 flex justify-center content-center">TimesUP!</span>

            <span className="mt-1 flex justify-center content-center">
                <Link to="/timesheet" className="mx-2 my-2">Timesheet</Link>
                <Link to="/dashboard" className="mx-2 my-2">Dashboard</Link>
            </span>
        </nav>
    )
}